# Kubernetes

## Setting up a lightweight kubernetes environment

### Master node
**Install k3s unlock rancher node**
> curl -sfL https://get.k3s.io | sh - 
**Check for Ready node, takes ~30 seconds**
> k3s kubectl get node 
**Run server**
> sudo k3s server &
**Kubeconfig is written to /etc/rancher/k3s/k3s.yaml**
sudo k3s kubectl get node

### Setup

**Create the unlock namespace**
> k3s kubectl create namespace unlock

**Storage**
> kubectl --namespace=unlock apply -f ./configuration/unlock-pv.yaml

> kubectl --namespace=unlock apply -f ./configuration/unlock-pvc.yaml

> kubectl --namespace=unlock apply -f ./configuration/docker-pv.yaml

> kubectl --namespace=unlock apply -f ./configuration/docker-pvc.yaml

**Keys**
> kubectl --namespace=unlock create secret generic unlock-secret \
    --from-literal=irodsHost='unlock-icat.irods.surfsara.nl' \
    --from-literal=irodsPort='1247' \
    --from-literal=irodsUserName='YourAccount' \
    --from-literal=irodsZone='unlock' \
    --from-literal=irodsAuthScheme='password' \
    --from-literal=irodsHome='/' \
    --from-literal=irodsCwd='/' \
    --from-literal=irodsPassword='A_VERY_SECURE_PASSWORD' \
    --from-literal=irodsSSL='CS_NEG_REQUIRE' \

### Add a node
**Install docker**

https://docs.docker.com/engine/install/ubuntu/

**Register node**
**NODE_TOKEN comes from the masternode. Execute masternode:**

> sudo cat /var/lib/rancher/k3s/server/node-token

**On a different node run the below command**
> curl -sfL https://get.k3s.io | K3S_URL=https://unlockrancher.wurnet.nl:6443 K3S_TOKEN=!!!!TOKEN_HERE!!!! sh -

### Dashboard access

**Get the token from the masternode**
> k3s kubectl -n kubernetes-dashboard create token admin-user

**start SSH tunnel**
> ssh unlockrancher.wurnet.nl -L 8001:localhost:8001 -N

**Access the web interface**

http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/workloads?namespace=unlock