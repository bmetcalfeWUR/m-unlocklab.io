# Workflows

Our workflows are written in such a way that they depend on available docker images. Therefor execution of our workflows do not need a lot dependencies even though there are many different tools used but rely (heavily) on docker and publically available images. We either use already available community images or we create and host them ourself when needed.

**Requirements:**

- Docker
- A cwl-runner (we use [cwltool](https://github.com/common-workflow-language/cwltool/))
- UNLOCK CWL repository: [https://gitlab.com/m-unlock/cwl/](https://gitlab.com/m-unlock/cwl/)

**Installation and usage**

Short introduction on how setup and use these workflows:<br>
[](../setup/setup.md) section.
<br>

These workflows are also published on the WorkflowHub: [https://workflowhub.eu/projects/16#workflows](https://workflowhub.eu/projects/16#workflows). On here you can see in more detail the in- and outputs and the steps involved.

Some are summarized here.

#### Workflow Metagenomics Assembly

[Workflowhub link](https://workflowhub.eu/workflows/367)

Workflow for assembly from Illumina reads and/or longreads. Customizable to a certain extend on which steps to run. (can also be used for isolates)<br>
Main steps involved:

- [Workflow Illumina Quality](#Workflow-Illumina-Quality)
- [Workflow Longread Quality](#Workflow-Longread-Quality)
- Assembly: SPAdes / Flye
- Short read polishing (Pilon)
- ONT read polishing (Medaka)
- QUAST (Assembly quality report)
- [Workflow Metagenomics Binning](Workflow-Metagenomics-Binning)
- [Worklow Metagenomics GEM](Worklow-Metagenomics-GEM)

#### Workflow Illumina Quality

[Workflowhub link](https://workflowhub.eu/workflows/336)

- FastQC quality plots (before and after filtering)
- fastp quality filtering
- Reference/contamination filtering (mapped or unmapped)
- Kraken2 taxonomic read classification (before and after)
- PhiX removal
- rRNA removal

#### Workflow Longreads Quality

[Workflowhub link](https://workflowhub.eu/workflows/337)

- NanoPlot Quality plot and reports (before and after filtering)
- Filtlong Longreads quality filtering
- Reference/contamination filtering (mapped or unmapped)
- Kraken2 taxonomic read classification

#### Workflow Metagenomics Binning

[Workflowhub link](https://workflowhub.eu/workflows/64)

- Metabat2 / MaxBin2 / SemiBin binning
- DAS Tool bin refinement
- EukRep (eukaryotic classification)
- CheckM bin quality
- BUSCO bin quality
- GTDB-Tk bin taxonomic classification

#### Workflow Metagenomic GEM

[Workflowhub link](https://workflowhub.eu/workflows/372)

***!! Important caveat with this workflow:***<br>
*The CarveMe, MEMOTA and SMETANA Docker container images that we use have the licenced CPLEX Optimizer build in. Therefor we can not make these images public unfortunately. This means this workflow will not work out-of-the-box. We did however make the Docker Build files available [here](https://gitlab.com/m-unlock/docker/-/tree/main/cwl/builder/docker)*

- Prodigal protein prediction
- CarveMe GEnome-scale Metabolic model reconstruction
- MEMOTE for metabolic model testing
- SMETANA Species METabolic interaction ANAlysis