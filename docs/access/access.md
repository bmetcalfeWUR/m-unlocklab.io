# Access

## How to access the UNLOCK FAIR Data Platform

The UNLOCK FAIR Data Platform provides an infrastructure for FAIR data and meta-data storage as well as a computational platform. Accepted projects will get an appointed project manager with whom the possibilities and usage of the the infrastructure will be discussed.

Once a project is accepted, the appointed project manager will contact the project members to discuss the project and the possibilities and usage of the UNLOCK FAIR Data Platform. A project group will be created in SRAM and the project manager will be able to add project members to the group. The project manager will also be able to add project members to the project in the UNLOCK FAIR Data Platform.

### SURF Resource Access Management (SRAM)

The UNLOCK FAIR Data Platform uses the authentication of SURFconext. This means that you need to be a member of a SURFconext identity provider (IdP) to be able to access the UNLOCK FAIR Data Platform. If you are not a member of a SURFconext IdP, you can request access to SURFconext via your home institution or create an account via EDUID. EduID is a Dutch initiative to create a single sign-on solution for Dutch educational institutions. You can create an account via [https://eduid.nl](https://eduid.nl).

### Access

The project manager from UNLOCK will create a project group in SRAM to which you then will be invited. Once you are invited you should receive an email with a link to accept the invitation. After accepting the invitation you will be able to log in to SRAM [http://sram.surf.nl](http://sram.surf.nl). 

![](../figures/sram-login.png)

### Token generation

Once you are logged in to SRAM you can generate a token to access the UNLOCK FAIR Data Platform. You can do this by clicking on the "Generate token" button. You can copy this token and use it to log in to the UNLOCK FAIR Data Platform. A token has a limited lifetime which is mentioned in the expiration date. You can generate a new token at any time. It is also possible to have multiple tokens at the same time as well.

![](../figures/sram-token.png)

### Username from SRAM

The username is created by SRAM using a combination of your first and last name. To find your username click on the dropdown button in the top right corner of the screen and click on "Profile". Your username is shown in the "SRAM internal username" field.

### Logging in to the UNLOCK FAIR Data Platform

Once you have your username and generated token you can log in to the UNLOCK FAIR Data Platform. There are different ways to access the UNLOCK FAIR Data Platform. You can access the UNLOCK FAIR Data Platform via the web interface at [http://data.m-unlock.nl](http://data.m-unlock.nl), via the iRODS command line interface or using webdav compatible software (e.g. Cyberduck).

### Connection details

The connection details for the UNLOCK FAIR Data Platform are:

| Field | Value |
| ----- | ----- |
| Host | `https://unlock-icat.irods.surfsara.nl/` |
| Port | `443` |
| User | `SRAM USERNAME` |
| Token | `SRAM TOKEN` |


#### Cyberduck

Cyberduck is the preferred way to upload large amounts of data to the system. It is a webdav compatible software that can be used to access the UNLOCK FAIR Data Platform. You can download Cyberduck from [https://cyberduck.io](https://cyberduck.io). Once you have installed Cyberduck you can add a new connection by clicking on the "Open Connection" button.

![](../figures/cyberduck-login.png)

#### Web interface
To use the web interface you go to [http://data.m-unlock.nl](http://data.m-unlock.nl) and click on the "iRODS Login" button. This will show you the login screen in your browser where you can log in to the FAIR Data Station. 

You can paste your token in the "Password" field and click on the "Login" button. This will log you in to the FAIR Data Station.

#### Command line interface

To use the command line interface you need to install the iRODS client. You can find the installation instructions for your operating system on the iRODS website [https://docs.irods.org/master/icommands/user/](https://docs.irods.org/master/icommands/user/).

For more information go to the [iRODS](../irods/irods.md) section.
