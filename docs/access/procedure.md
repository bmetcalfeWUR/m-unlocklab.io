# Procedure

The **UNLOCK FAIR Data Platfrom** provides an infrastructure for **FAIR data** and **meta-data** storage as well as a **computational platform**. Accepted projects will get an appointed project manager with whom the possibilities and usage of the the infrastructure will be discussed.

## Project Registration

To get access to the **UNLOCK FAIR Data Platfrom** users are asked to provide information (metadata) about their **Project** and related **Investigations** and **Studies** following the ISA standard.

This information will initially be captured off-line using a **specific preformatted Excel sheet**. This excel is empty except for Project level meta-data and a number of the predefined **Headers** for data specific meta-data. These headers should not be changed as they are part of the UNLOCK infrastructure ontology.  

As not every project is the same we made a Metadata Registration Web-Form that will generate a customized excel template. This contains specific metadata about the Project/investigation/Study, envisioned experiments and generated data. General project related information (metadata) should be entered in this web-form. This includes: Project information, research questions, experimental setup and how the the data is or will be generated. Users are encouraged to capture as much information (metadata) as possible.

After Web-form completion users download their custom **project specific template Excel sheet** and start registering specific meta-data of their samples and assays. The filled in Excel sheet is subsequently uploaded to the Unlock website and automatically checked for common mistakes and followed by a final manual check by the data steward. After which following the Unlock structure and the data-related project specific meta-data is automatically extracted and used to set up the data/meta-data folder structure  

### The Unlock Project data management structure

![UNLOCK datamanagement](../figures/unlock_isa_2.png)

More information and usage about the metadata registration see [FAIR Data Station](../fairdatastation/webform.md)

### Submitting metadata and (raw) assay data

Submittion of generated (raw) assay data such as sequencing data in this environment can be done via multiple ways and will be discussed with the data steward.


### Getting access to the infrastructure and your data

For each project and it's investigations users are asked who is involved in the project/investigation and who needs access to the data. For each given person who requires access to the data will get an account with **read only access** to the files and (meta)data generated within the project its investigation(s). 

The data and metadata will be stored in a secure iRODS instance and can be accessed via different ways (e.g. file explorer and web browser and ). See [iRODS](../irods/usage.md)

Next to the iRODS storage, meta-data of the provided and generated data files and other project specific meta data will also be accumulated in a RDF resource.
