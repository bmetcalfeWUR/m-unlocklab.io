
# WebDav

It is possible to use webdav in iRODS by placing the ***URL*** that is normally used for the authentication procedure in your web browser. It should prompt a username / password panel which when succesfully will show you the iRODS instance content.

It is also possible to mount the iRODS instance as a network drive. Select the tab that corresponds to your environment for more details.

````{tabbed} Mac
    On a mac you can mount webdav folders using the Finder. When you have a Finder window open press `CMD + k`. This will provide a popup in which you can provide the webdav URL.    
    ```{figure} ./images/webdav_mac.png 
    ---
    name: Mac-webdav
    ---
    ```
    **When prompted for a username / password provide the credentials used to access the webdav system.**
````

````{tabbed} Linux/Ubuntu
    In linux you can mount other locations such as webdav using your file browser. Simply click on 'Other Locations' and provide the address of your webdav server. Note, if the webdav server uses SSL-encryption you might have to use the protocol *davs*, otherwise use *dav*:    
    ```{figure} ./images/DavRodsMount-linux2.png 
    ---
    name: Ubuntu-webdav
    ---
    ```
    **When prompted for a username / password provide the credentials used to access the webdav system.**
````

````{tabbed} Windows
    When using windows, you have to mount the WebDAV system as a network drive.
    
    - Start your file browser
    
    - Right click on the computer icon

    - Map network drive

    - Paste the entire WebDAV URL in the Folder
    ```{figure} ./images/webdav_windows.png 
    ---
    name: Windows-webdav
    ---
    ```
    **Click finish and when requested for your credentials use the credentials to access the webdav system.**
````

````{tabbed} Cyberduck

    There are other applications which you can use to connect to the webdav endpoint such as for example Cyberduck.

    - Paste the webdav address in the Quick connect bar and it should connect to your webdav instance.

    - When requested for your credentials use the credentials to access the webdav system.
    ```{figure} ./images/cyberduck.png 
    ---
    name: Cyberduck-webdav
    ---
    ```
    **When prompted for a username / password provide the credentials used to access the webdav system.**
````

# iCommands

Currently the latest version of iCommands is only available through a Linux environment.
The easiest approach to have icommands available on your system is through Docker.

To install docker look at your operating system below:

````{tabbed}  Windows 10

 Windows 10 docker installation instructions

 0. Administrator rights are required for this procedure. If you do not have these, please request them by sending an email to servicedesk.it@wur.nl
 1. Open a Windows powershell window as an administrator. Run the following command to enable the Windows subsystem for linux:
 `Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux`. __Reboot as required.__
 2. Open the Microsoft store by opening or searching for it via the start menu. In the store search for Ubuntu. Select the "Ubuntu 18.04" app and download it.
 3. After downloading and installing, the app should be ready to go. Start it via the start menu by clicking on the tile / searching for it. The app will continue installing. After that, create a username/ password according to the instructions that appear.
 4. You are now at a Linux terminal. You can follow the [general iRods tutorial](https://github.com/irods/irods_training/raw/master/beginner/irods_beginner_training_2019.pdf) from this point on (start from chapter 4 to get practical).
````

````{tabbed} Mac OSX

    To install docker for mac please download and install the docker application from https://www.docker.com/products/docker-desktop

    When docker is installed we have prepared a docker image    with the iCommands.
````

````{tabbed} Ubuntu

    It is possible to install the iCommands directly within ubuntu. 

    To install docker for ubuntu please follow the
    instructions on this website: 
    https://docs.docker.com/engine/install/ubuntu/
````

## Vagrant

Vagrant is the command line utility for managing the lifecycle of virtual machines. Isolate dependencies and their configuration within a single disposable and consistent environment. Vagrant is distributed as a binary package for all supported platforms and architectures. You can also compile Vagrant from source, which is covered in the README.

### Installation

The documentation for the installation of vagrant is available at:
    <https://developer.hashicorp.com/vagrant/install>

#### macOS package manager

`brew tap hashicorp/tapbrew install hashicorp/tap/hashicorp-vagrant`

#### Windows

For windows you can download the binary package at:
<https://developer.hashicorp.com/vagrant/install#windows>

#### Linux

For linux there are several flavours available at:
<https://developer.hashicorp.com/vagrant/install#linux>
However it is very likely that iCommands is directly available for you for your operating system.

### Initialize a project directory

*From the documentation at Vagrant…*

The first step to configure any Vagrant project is to create a Vagrantfile. The Vagrantfile allows you to:

- Mark the root directory of your project. Many of the configuration options in Vagrant are relative to this root directory.
- Describe the kind of machine and resources you need to run your project, as well as what software to install and how you want to access it.

**Create a directory**

- Mac/Linux: mkdir vagrant_getting_started
- Windows: Use file explorer to create a folder

**Initialize the project**

Make sure you are inside your project folder using the command line interface

`vagrant init vagrant init generic/ubuntu2204`

```{code-block} bash
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
```

There are many other vagrant images available but for this documentation we stick to a relatively recent ubuntu 22.04 image. Other images can be found at <https://app.vagrantup.com/>

**Boot an environment**

Now that you have initialized your project and configured a box for it to use, it is time to boot your first Vagrant environment.

`vagrant up`

```{code-block} bash
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Box 'generic/ubuntu2204' could not be found. Attempting to find and install...
    default: Box Provider: virtualbox
    default: Box Version: >= 0
==> default: Loading metadata for box 'generic/ubuntu2204'
    default: URL: <https://vagrantcloud.com/api/v2/vagrant/generic/ubuntu2204>
==> default: Adding box 'generic/ubuntu2204' (v4.3.12) for provider: virtualbox (amd64)
    default: Downloading: <https://vagrantcloud.com/generic/boxes/ubuntu2204/versions/4.3.12/providers/virtualbox/amd64/vagrant.box>
Progress: 3% (Rate: 12.0M/s, Estimated time remaining: 0:03:08)

==> default: Machine booted and ready!
```

**SSH into the machine**

You will not actually see anything though, since Vagrant runs the virtual machine without a UI. To prove that it is running, you can SSH into the machine:

`vagrant ssh`

> vagrant@ubuntu2204:~$

**Installing iCommands**

```{code-block} bash
wget -qO - <https://packages.irods.org/irods-signing-key.asc> | sudo apt-key add -
echo "deb [arch=amd64] <https://packages.irods.org/apt/> $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/renci-irods.list
sudo apt-get update
sudo apt-get install irods-icommands
```

### Docker

-----

To start the docker container with the icommands available you can use this command:

```
docker run -it --entrypoint /bin/bash docker-registry.wur.nl/m-unlock/docker/irods:latest
```

As this is a clean docker instance it does not contain any authentication documentation. You can create the required irods_environment.json file inside the docker instance but upon starting a new instance this information is lost.

The best approach would be to exit the docker container and create an irods folder containing the irods_envrionment.json file. There are also additional features within this json file that ensures that your connection is encrypted and that you do not need to provide all access information each time you start the docker container.

- Create an `irods` folder
- Create an irods_environment.json file in the `irods` folder with the following content

```
{
    "irods_user_name": "<USERNAME>",
    "irods_zone_name": "unlock",
    "irods_host": "unlock-icat.irods.surfsara.nl",
    "irods_port": 1247,
    "irods_client_server_negotiation": "request_server_negotiation",
    "irods_client_server_policy": "CS_NEG_REQUIRE",
    "irods_encryption_key_size": 32,
    "irods_encryption_salt_size": 8,
    "irods_encryption_num_hash_rounds": 16,
    "irods_encryption_algorithm": "AES-256-CBC",
    "irods_ssl_verify_server": "cert"
 }
```

Make sure that you change your USERNAME, ZONE and HOSTNAME to the correct values.

When starting the docker container it is essential that the irods folder is mounted to the internal location of `~/irods/`. The mounting of the folder is achieved via the `-v` parameter.

```
docker run -it -v /PATH/TO/IRODS/FOLDER:/root/.irods docker-registry.wur.nl/m-unlock/docker/irods:latest
```

When the docker container has succesfully started you can login via the `iinit command`

```
> iinit
Enter your current iRODS password:
```

When that is succcessful the iCommands are available for use. See `ihelp` or for more information.

<!-- 
## Python

## Accessing only data in iRODS

### iRODS tickets

## Publishing data with iRODS

## Anonymous access 
-->
